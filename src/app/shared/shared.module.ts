import { SafeHtmlPipe } from './safe-html/safe-html.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypingTestAoeuComponent } from './typing-test-aoeu/typing-test-aoeu.component';

const SHARED: any[] = [SafeHtmlPipe, TypingTestAoeuComponent];

@NgModule({
  declarations: [...SHARED],
  imports: [CommonModule],
  exports: [...SHARED],
})
export class SharedModule {}
