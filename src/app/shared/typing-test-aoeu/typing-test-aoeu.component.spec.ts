import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypingTestAoeuComponent } from './typing-test-aoeu.component';

describe('TypingTestAoeuComponent', () => {
  let component: TypingTestAoeuComponent;
  let fixture: ComponentFixture<TypingTestAoeuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypingTestAoeuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypingTestAoeuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
