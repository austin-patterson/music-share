import { YtSnippetBaseModel } from './yt-snippet-base.model';
export class YtPlaylistModel {
  kind: 'youtube#playlist';
  etag: string; //etag
  id: string;
  snippet: YtPlaylistSnippetModel;
  status?: {
    privacyStatus: string;
  };
  contentDetails?: {
    itemCount: number; //unsigned integer
  };
  player: {
    embedHtml: string;
  };
  localizations: {
    //language code
    (key: any): {
      title: string;
      description: string;
    };
  };
}

export class YtPlaylistSnippetModel extends YtSnippetBaseModel {
  channelTitle: string;
  tags: string[];
  defaultLanguage: string;
  localized: {
    title: string;
    description: string;
  };
}
