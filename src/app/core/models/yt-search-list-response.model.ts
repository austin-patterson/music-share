import { YtSearchResultModel } from './yt-search-result.model';

export class YtSearchListResponseModel {
  kind: 'youtube#searchListResponse';
  etag: string;
  nextPageToken: string;
  regionCode: string;
  pageInfo: {
    totalResults: number; //unsigned int
    resultsPerPage: number; //unsigned int
  };
  items: YtSearchResultModel[];
}
