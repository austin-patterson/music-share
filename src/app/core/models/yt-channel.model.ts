import { YtSnippetBaseModel } from './yt-snippet-base.model';

export class YtChannelModel {
  kind: 'youtube#channel';
  etag: string;
  id: string;
  snippet: (YtSnippetBaseModel & );
  contentDetails?: {
    relatedPlaylists: {
      likes: string;
      favorites: string;
      uploads: string
    }
  };
  statistics?: {
    viewCount: number; //unsigned long
    subscriberCount: number; //unsigned long (this value is rounded to three significant figures)
    hiddenSubscriberCount: boolean;
    videoCount: number //unsigned long
  };
  topicDetails?: {
    topicIds: string[];
    topicCategories: string[]
  };
  status?: {
    privacyStatus: string;
    isLinked: boolean;
    longUploadsStatus: string;
    madeForKids: boolean;
    selfDeclaredMadeForKids: boolean
  };
  brandingSettings?: {
    channel: {
      title: string;
      description: string;
      keywords: string;
      defaultTab: string;
      trackingAnalyticsAccountId: string;
      moderateComments: boolean;
      showRelatedChannels: boolean;
      showBrowseView: boolean;
      featuredChannelsTitle: string;
      featuredChannelsUrls: string[];
      unsubscribedTrailer: string;
      profileColor: string;
      defaultLanguage: string;
      country: string
    };
    watch: {
      textColor: string;
      backgroundColor: string;
      featuredPlaylistId: string
    }
  };
  auditDetails?: {
    overallGoodStanding: boolean;
    communityGuidelinesGoodStanding: boolean;
    copyrightStrikesGoodStanding: boolean;
    contentIdClaimsGoodStanding: boolean
  };
  contentOwnerDetails?: {
    contentOwner: string;
    timeLinked: string //datetime
  };
  localizations?: {
    (key: any): {
      title: string;
      description: string
    }
  }
}

export class YtChannelSnippetModel extends YtSnippetBaseModel {
  defaultLanguage?: string;
  localized?: {
    title: string;
    description: string
  };
  country?: string
}