import { YtSnippetBaseModel } from './yt-snippet-base.model';
export class YtVideoModel {
  kind: 'youtube#video';
  etag: string; //etag
  id: string;
  snippet: YtVideoSnippetModel;
  contentDetails?: {
    duration: string;
    dimension: string;
    definition: string;
    caption: string;
    licensedContent: boolean;
    regionRestriction: {
      allowed: [
        string
      ];
      blocked: [
        string
      ]
    };
    contentRating: {
      acbRating: string;
      agcomRating: string;
      anatelRating: string;
      bbfcRating: string;
      bfvcRating: string;
      bmukkRating: string;
      catvRating: string;
      catvfrRating: string;
      cbfcRating: string;
      cccRating: string;
      cceRating: string;
      chfilmRating: string;
      chvrsRating: string;
      cicfRating: string;
      cnaRating: string;
      cncRating: string;
      csaRating: string;
      cscfRating: string;
      czfilmRating: string;
      djctqRating: string;
      djctqRatingReasons: string[];
      ecbmctRating: string;
      eefilmRating: string;
      egfilmRating: string;
      eirinRating: string;
      fcbmRating: string;
      fcoRating: string;
      fmocRating: string;
      fpbRating: string;
      fpbRatingReasons: string[];
      fskRating: string;
      grfilmRating: string;
      icaaRating: string;
      ifcoRating: string;
      ilfilmRating: string;
      incaaRating: string;
      kfcbRating: string;
      kijkwijzerRating: string;
      kmrbRating: string;
      lsfRating: string;
      mccaaRating: string;
      mccypRating: string;
      mcstRating: string;
      mdaRating: string;
      medietilsynetRating: string;
      mekuRating: string;
      mibacRating: string;
      mocRating: string;
      moctwRating: string;
      mpaaRating: string;
      mpaatRating: string;
      mtrcbRating: string;
      nbcRating: string;
      nbcplRating: string;
      nfrcRating: string;
      nfvcbRating: string;
      nkclvRating: string;
      oflcRating: string;
      pefilmRating: string;
      rcnofRating: string;
      resorteviolenciaRating: string;
      rtcRating: string;
      rteRating: string;
      russiaRating: string;
      skfilmRating: string;
      smaisRating: string;
      smsaRating: string;
      tvpgRating: string;
      ytRating: string
    };
    projection: string;
    hasCustomThumbnail: boolean
  };
  status: {
    uploadStatus: string;
    failureReason: string;
    rejectionReason: string;
    privacyStatus: string;
    publishAt: string; //datetime
    license: string;
    embeddable: boolean;
    publicStatsViewable: boolean;
    madeForKids: boolean;
    selfDeclaredMadeForKids: boolean
  };
  statistics: {
    viewCount: number; //unsigned long
    likeCount: number; //unsigned long
    dislikeCount: number; //unsigned long
    favoriteCount: number; //unsigned long
    commentCount: number //unsigned long
  };
  player: {
    embedHtml: string;
    embedHeight: number; //long
    embedWidth: number //long
  };
  topicDetails: {
    topicIds: [
      string
    ];
    relevantTopicIds: [
      string
    ];
    topicCategories: [
      string
    ]
  };
  recordingDetails: {
    recordingDate: string //datetime
  };
  fileDetails: {
    fileName: string;
    fileSize: number; //unsigned long
    fileType: string;
    container: string;
    videoStreams: [
      {
        widthPixels: number; //unsigned integer
        heightPixels: number; //unsigned integer
        frameRateFps: number; //double
        aspectRatio: number //double
        codec: string;
        bitrateBps: number; //unsigned long
        rotation: string;
        vendor: string
      }
    ];
    audioStreams: [
      {
        channelCount: number; //unsigned integer
        codec: string;
        bitrateBps: number; //unsigned long
        vendor: string
      }
    ];
    durationMs: number; //unsigned long
    bitrateBps: number; //unsigned long
    creationTime: string
  };
  processingDetails: {
    processingStatus: string;
    processingProgress: {
      partsTotal: number; //unsigned long
      partsProcessed: number; //unsigned long
      timeLeftMs: number //unsigned long
    };
    processingFailureReason: string;
    fileDetailsAvailability: string;
    processingIssuesAvailability: string;
    tagSuggestionsAvailability: string;
    editorSuggestionsAvailability: string;
    thumbnailsAvailability: string
  };
  suggestions: {
    processingErrors: [
      string
    ];
    processingWarnings: [
      string
    ];
    processingHints: [
      string
    ];
    tagSuggestions: [
      {
        tag: string;
        categoryRestricts: [
          string
        ]
      }
    ];
    editorSuggestions: [
      string
    ]
  };
  liveStreamingDetails: {
    actualStartTime: string; //datetime
    actualEndTime: string; //datetime
    scheduledStartTime: string; //datetime
    scheduledEndTime: string; //datetime
    concurrentViewers: number; //unsigned long
    activeLiveChatId: string
  };
  localizations: {
    // country code
    (key: any): {
      title: string;
      description: string
    }
  }
}

export class YtVideoSnippetModel extends YtSnippetBaseModel {
  channelTitle: string;
  tags: string[];
  categoryId: string;
  liveBroadcastContent: 'upcoming' | 'live' | 'none';
  defaultLanguage: string;
  localized: {
    title: string;
    description: string
  };
  defaultAudioLanguage: string
}