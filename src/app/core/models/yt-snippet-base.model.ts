export class YtSnippetBaseModel {
  title: string;
  description: string;
  customUrl?: string;
  publishedAt: string; //datetime (ISO 8601)
  channelId: string;
  channelTitle: string;
  thumbnails: {
    default: {
      url: string;
      width: number;
      height: number;
    };
    medium?: {
      url: string;
      width: number;
      height: number;
    };
    high?: {
      url: string;
      width: number;
      height: number;
    };
    standard?: {
      url: string;
      width: number;
      height: number;
    };
    maxres?: {
      url: string;
      width: number; //unsigned int
      height: number; //unsigned int
    };
  };
}
