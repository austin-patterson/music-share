import { YtVideoModel } from './yt-video.model';

export class YtVideoListResponseModel {
  kind: 'youtube#videoListResponse';
  etag: string; //etag
  nextPageToken: string;
  prevPageToken: string;
  pageInfo: {
    totalResults: number; //integer
    resultsPerPage: number; //integer
  };
  items: YtVideoModel[];
}
