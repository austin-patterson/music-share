import { YtPlaylistModel } from './yt-playlist.model';

export class YtPlaylistResponseModel {
  kind: 'youtube#playlistListResponse';
  etag: string; //etag
  nextPageToken: string;
  prevPageToken: string;
  pageInfo: {
    totalResults: number; //integer
    resultsPerPage: number; //integer
  };
  items: YtPlaylistModel[];
}
