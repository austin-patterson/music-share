import { YtSnippetBaseModel } from './yt-snippet-base.model';
export class YtSearchResultModel {
  kind: 'youtube#searchResult';
  etag: string;
  id: {
    kind: string;
    playlistId?: string;
    videoId?: string;
    channelId?: string;
  };
  snippet: YtSearchResultSnippet;
}

export class YtSearchResultSnippet extends YtSnippetBaseModel {
  liveBroadcastContent: 'upcoming' | 'live' | 'none';
}
