import { YoutubePlayerComponent } from './youtube-player/youtube-player.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { JukeboxPageComponent } from './jukebox-page/jukebox-page.component';

@NgModule({
  declarations: [YoutubePlayerComponent, JukeboxPageComponent],
  imports: [CommonModule, MaterialModule],
  exports: [JukeboxPageComponent],
})
export class JukeboxModule {}
