import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-youtube-player',
  templateUrl: './youtube-player.component.html',
  styleUrls: ['./youtube-player.component.scss']
})
export class YoutubePlayerComponent implements OnInit {
  private BASE_SRC_URL = 'https://www.youtube.com/embed/';

  // TODO: make this dynamic!
  // https://www.youtube.com/embed/pO8v1iWVTIM?autoplay=1
  src: string;
  autoplay = true;
  videoId = 'pO8v1iWVTIM';

  playlistSrc =
    'http://www.youtube.com/embed/videoseries?list=PL81WJHi347jgBKaQiRFKerodI5IafJj4s';

  playlistHtmlCode =
    '\u003ciframe width="640" height="360" src="http://www.youtube.com/embed/videoseries?list=PL81WJHi347jgBKaQiRFKerodI5IafJj4s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen\u003e\u003c/iframe\u003e';

  updateSrc(): void {
    let newSrc = this.BASE_SRC_URL + this.videoId;

    if (this.autoplay) newSrc += '?autoplay=1';

    // fianlly
    this.src = newSrc;
  }

  constructor() {}

  ngOnInit(): void {
    this.updateSrc();
  }

}
