import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JukeboxPageComponent } from './modules/jukebox/jukebox-page/jukebox-page.component';
import { TypingTestAoeuComponent } from './shared/typing-test-aoeu/typing-test-aoeu.component';

const routes: Routes = [
  { path: 'jukebox', component: JukeboxPageComponent },
  // { path: 'jukebox', loadChildren: 'jukebox.module#JukeboxModule' },
  { path: 'typing-test', component: TypingTestAoeuComponent },
  { path: '', pathMatch: 'full', redirectTo: 'typing-test' },
  { path: '**', redirectTo: 'typing-test' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
